class Output {
    constructor() {
        this.outputFrame = $("#text-output")
        this.inventoryElem = $('#inventory')
    }

    addOutput(text) {
        this.outputFrame.append(
            $("<div/>", {"class": "outputElem"}).html(text)
        )
        //Scrolls to bottom if text is past bottom of screen
        let element = document.getElementById("text-output");
        element.scrollTop = element.scrollHeight - element.clientHeight;
    }
}

class Puzzles {
    constructor(state) {
        this.studyLockSolved = false
        this.studyLockRun()
        this.state = state;
    }

    studyLockRun() {
        let code = [0, 4, 2];
        $("#close-study-lock").click(() => {
            $("#study-lock-wrapper").css("display", "none");
            this.state.setState();
        })
        $("#enter-study-code").click(() => {
            let num0 = $("#lock-0-num").text();
            let num1 = $("#lock-1-num").text();
            let num2 = $("#lock-2-num").text();
            let message = $("#open-attempt-message")
            if (num0 == code[0] && num1 == code[1] && num2 == code[2]) {
                $("#study-lock").css("background-image", "url(\"./static/images/lock-unlocked.svg\"");
                message.text("Lock Opened!");
                this.studyLockSolved = true;
            } else {
                message.text("Nothing Happens");
                setTimeout(() => message.text(""), 1000)
            }
        })
        for (let i=0; i<3; i++) {
            let discDec = $("#lock-"+i+"-dec");
            let discInc = $("#lock-"+i+"-inc");
            let discNum = $("#lock-"+i+"-num");
            discDec.click(() => {
                if (discNum.text() == 0) {
                    discNum.empty();
                    discNum.text("9");
                } else {
                    let newVal = discNum.text() - 1;
                    discNum.empty()
                    discNum.text(newVal);
                }
            })
    
            discInc.click(() => {
                if (discNum.text() == 9) {
                    discNum.empty();
                    discNum.text("0");
                } else {
                    let newVal = parseInt(discNum.text()) + 1;
                    discNum.empty()
                    discNum.text(newVal);
                }
            })
        }
    }
}

class Collectable {
    constructor(name, description, lookDescription, aliases, gameState, showOnRoomLook=false, elemToShowOnExamine=false) {
        this.name = name;
        this.description = description;
        this.lookDescription = lookDescription;
        this.aliases = aliases;
        this.gameState = gameState;
        this.showOnRoomLook = showOnRoomLook;
        this.elemToShowOnExamine = elemToShowOnExamine;
    }

    getDescription() {
        return this.description;
    }

    getLookDescription() {
        return this.lookDescription;
    }

    onExamine() {
        $(this.elemToShowOnExamine).css("display", "flex"); 
    }
}

class Inspectable { 
    constructor(name, description, lookDescription, aliases, gameState, showOnRoomLook=false, elemToShowOnExamine=false) {
        this.name = name;
        this.description = description;
        this.aliases = aliases;
        this.showOnRoomLook = showOnRoomLook;
        this.lookDescription = lookDescription;
        this.gameState = gameState;
        this.elemToShowOnExamine = elemToShowOnExamine;
    }

    getDescription() {
        return this.description;
    }

    getLookDescription() {
        return this.lookDescription;
    }

    onExamine() {
        $(this.elemToShowOnExamine).css("display", "flex"); 
    }


}

class Room {
    constructor(name, description, visitedDescription, markerPos, aliases=[], inspectables={}, collectables={}, containers={}, state, floor="down", stateToAccess=0, deniedAccessMessage="", doorPos={}, doorCrossElem="") {
        this.downstairsMarker = $('#downstairs-marker');
        this.upstairsMarker = $('#upstairs-marker');
        this.name = name;
        this.description = description;
        this.visited = false;
        this.visitedDescription = visitedDescription;
        this.aliases = aliases;
        this.inspectables = inspectables;
        this.collectables = collectables;
        this.containers = containers;
        this.gameState = state;
        this.floor = floor;
        this.markerPos = markerPos;
        this.stateToAccess = stateToAccess;
        this.deniedAccessMessage = deniedAccessMessage;
        this.doorPos = doorPos;
        this.doorCrossElem = $(doorCrossElem);
    }

    getDescription() {
        if (!this.visited) {
            this.visited = true;
            return this.description;
        } 
        return this.visitedDescription;
    }

    getAllInspectables() {
        return this.inspectables;
    }

    getAllCollectables() {
        return this.collectables;
    }

    getListInspectables() {
        return this.inspectables;
    }

    editInspectableLookDescription(inspectable, description) {
        this.inspectables[inspectable].lookDescription = description;
    }


    getRoomDescription() {
        let roomDescription = this.getDescription();
        let output = [roomDescription];
        Object.keys(this.inspectables).forEach(key => {
            let inspectable = this.inspectables[key];
            if (inspectable.showOnRoomLook) {
                output.push(inspectable.description);
            }
        })
        Object.keys(this.collectables).forEach(key => {
            let collectable = this.collectables[key];
            if (collectable.showOnRoomLook && !this.gameState.inInventory(collectable)) {
                output.push(collectable.description);
            }
        })
        return output;
    }

    setMarker() {
        if (this.floor == "up") {
            this.downstairsMarker.css("display", "none")
            this.upstairsMarker.css(this.markerPos);
            this.upstairsMarker.css("display", "inline-block");
        } else if (this.floor == "down") {
            this.upstairsMarker.css("display", "none")
            this.downstairsMarker.css(this.markerPos);
            this.downstairsMarker.css("display", "inline-block");
        }
    }

    showDoorCross() {
        this.doorCrossElem.css("display", "block");
        this.doorCrossElem.css(this.doorPos);
    }

    hideDoorCross() {
        this.doorCrossElem.css("display", "none");
    }
}

class State {
    constructor(state, output, puzzles=false, playerInventory={}, playerRoom=false, puzzlesCompleted=[], mostRecentItem="") {
        this.state = state;
        this.stateRun = false;
        this.output = output;
        this.puzzles = puzzles;
        this.playerInventory = playerInventory;
        this.mostRecentItem = mostRecentItem;
        this.playerRoom = playerRoom;
        this.puzzlesCompleted = puzzlesCompleted;

    }
    
    updateInventory() {
        let inventoryElem = $("#inventory");
        inventoryElem.empty();
        inventoryElem.text("Inventory:");
        Object.keys(this.playerInventory).forEach(key => {
            inventoryElem.append($("<div/>", { "class": "inventoryElem", "text": "> " + this.playerInventory[key].name}));
        });
    }

    addToInventory(item) {
        this.playerInventory[item.name] = item;
        this.mostRecentItem = item;
        this.updateInventory();
    }

    removeFromInventory(item) {
        Object.keys(this.playerInventory).forEach(key => {
            let invItem = this.playerInventory[key];
            if (invItem == item) {
                delete this.playerInventory[key];
            }
        })
        this.updateInventory();
    }

    inInventory(item) {
        return Object.values(this.playerInventory).indexOf(item) != -1;
    }

    setMostRecentItem(item) {
        this.mostRecentItem = item;
    }

    getMostRecentItem() {
        return this.mostRecentItem;
    }

    addToPuzzlesCompleted(puzzle) {
        this.puzzlesCompleted.push(puzzle);
    }

    getMostRecentPuzzle() {
        return this.puzzlesCompleted[this.puzzlesCompleted.length-1];
    }

    setCurrentRoom(room) {
        this.playerRoom = room;
    }

    getCurrentRoom() {
        return this.playerRoom;
    }

    getCurrentRoomDescription() {
        return this.playerRoom.getRoomDescription();
    }

    getCurrentRoomAliases() {
        return this.playerRoom.aliases;
    }

    setState() {
        //TODO: picking up map should show map reguardless of state
        let stateNumber = this.getStateNumber();
        console.log("checking state");
        if (!this.getMostRecentItem()) {
            return this;
        } else if (this.getMostRecentItem().name === "Downstairs Map" && stateNumber != 1 && stateNumber < 1) {
            return new State(1, this.output,this.puzzles, this.playerInventory, this.playerRoom, this.puzzlesCompleted, this.mostRecentItem);
        } else if (this.getMostRecentPuzzle() === "Note" && stateNumber != 2 && stateNumber < 2) {
            return new State(2, this.output, this.puzzles, this.playerInventory, this.playerRoom, this.puzzlesCompleted, this.mostRecentItem);
        } else  if (this.puzzles.studyLockSolved && stateNumber != 3 && stateNumber < 3) {
            return new State(3, this.output, this.puzzles, this.playerInventory, this.playerRoom, this.puzzlesCompleted, this.mostRecentItem);
        } else  if (this.getMostRecentPuzzle() === "Cats" && stateNumber != 4 && stateNumber < 4) {
            return new State(4, this.output, this.puzzles, this.playerInventory, this.playerRoom, this.puzzlesCompleted, this.mostRecentItem);
        } else  if (this.getMostRecentPuzzle() === "Gin" && stateNumber != 5 && stateNumber < 5) {
            return new State(5, this.output, this.puzzles, this.playerInventory, this.playerRoom, this.puzzlesCompleted, this.mostRecentItem);
        } else  if (this.getMostRecentPuzzle() === "Sloe Gin" && stateNumber != 6 && stateNumber < 6) {
            return new State(6, this.output, this.puzzles, this.playerInventory, this.playerRoom, this.puzzlesCompleted, this.mostRecentItem);
        } else  if (this.getMostRecentPuzzle() === "Lounge" && stateNumber != 7 && stateNumber < 7) {
            return new State(7, this.output, this.puzzles, this.playerInventory, this.playerRoom, this.puzzlesCompleted, this.mostRecentItem);
        } else  if (this.getMostRecentPuzzle() === "UV" && stateNumber != 8 && stateNumber < 8) {
            return new State(8, this.output, this.puzzles, this.playerInventory, this.playerRoom, this.puzzlesCompleted, this.mostRecentItem);
        } else  if (this.getMostRecentPuzzle() === "Front" && stateNumber != 9 && stateNumber < 9) {
            return new State(9, this.output, this.puzzles, this.playerInventory, this.playerRoom, this.puzzlesCompleted, this.mostRecentItem);
        } else  if (this.getMostRecentPuzzle() === "Complete" && stateNumber != 10 && stateNumber < 10) {
            return new State(10, this.output, this.puzzles, this.playerInventory, this.playerRoom, this.puzzlesCompleted, this.mostRecentItem);
        } else {
            return this;
        }
    }

    getStateNumber() {
        return this.state;
    }
}

class Container {
    constructor(name, description, contents, nameList, lookDescription, aliases, gameState, showOnRoomLook=false, elemToShowOnExamine=false) {
        this.name = name;
        this.description = description;
        this.contents = contents;
        this.nameList = nameList;
        this.lookDescription = lookDescription;
        this.aliases = aliases;
        this.gameState = gameState;
        this.showOnRoomLook = showOnRoomLook;
        this.elemToShowOnExamine = elemToShowOnExamine;
    }

    updateName() {
        let tempName = this.name;
        if (this.nameList.length != 1) {
            tempName = this.nameList[0] + " (";
            for (let i=1; i<this.nameList.length; i++) {
                if (i < this.nameList.length-1) {
                    tempName += this.nameList[i] + " + ";
                } else {
                    tempName += this.nameList[i] + ")";
                }
            }
        }
        this.name = tempName;
        this.gameState.updateInventory()
    }
}

class Game {
    constructor() {
        this.input = $('#text-input');
        this.output = new Output();
        this.state = new State(0, this.output);
        this.puzzles = new Puzzles(this.state);
        this.state.puzzles = this.puzzles;
        

        this.rooms = {
            HALL: new Room('the Hall',"The entrance to the house. Around you are several doors leading to various parts of the house. ", "The entrance to the house. Around you are several doors leading to various parts of the house. ",
            {"top": "70%", "left": "35%"},
            ["hall", "hallway"],
            {"radiator": new Inspectable("Radiator", "a warm <span class='a'>radiator</span> hanging upon the wall", "A radiator that is gently warming the space around it.", ["radiator"], this.state, true),
            "cats": new Inspectable("Cats", "two angry, hungry, looking cats, sitting on the stairs, one of whom looks slightly tarded", "You look at the cats a bit closer and see that one of them is attempting to defend the stairs from its own tail. You try to skim past while it's distracted, but it claws at your weak shins, forcing you back down the stairs.", ["cats", "cat"], this.state, true),
            "study door": new Inspectable("Study Door", "the <span class='a'>study door</span>", "The door to the study, with a combination lock, you'll need to find a code to get the <span class='a'>study lock</span> open.", ["study door", "Study Door"], this.state, true),
            "study lock": new Inspectable("Study Lock", "A lock keeping the study door shut.", "A combination lock that is keeping you from entering the Study.", ["study lock", "Study Lock"], this.state, false, "#study-lock-wrapper"),
            "lounge door": new Inspectable("Lounge Door", "the <span class='a'>lounge door</span>", "The door to the lounge, you give it a push but it appears to be locked. There's a <span class='a'>lounge lock</span> you suspect a <span class='a'>lounge key</span> may fit in.", ["lounge door", "Lounge Door"], this.state, true),
            "lounge lock": new Inspectable("Lounge Lock", "", "A keyhole in the lounge door, you suspect a key may be they key to opening this lock.", ["lounge lock"], this.state, false),
            "front door": new Inspectable("Front Door", "the <span class='a'>front door</span>", "The front door, your way out to freedom. You'll need to find a way to unlock the <span class='a'>front door lock</span>", ["front door"], this.state, true),
            "front door lock": new Inspectable("Front Door Lock", "", "The only thing stopping you from escaping this weird hellhole.", ["front door lock"], this.state, false)},
            {"downstairs map": new Collectable("Downstairs Map", "a scrap of <span class='a'>paper</span> with some rough scribblings on it", "What appears to be a crudly drawn map of the downstairs.", ["map", "paper"], this.state, true),
            },
            {},
            this.state,
            "down"
            ),

            STUDY: new Room('the Study', "A room with many files about every pipe and valve known to man. On the desk is the reciept for a flight to Fuerteventura. The Ridgers' Parents must be away. ",
            "A standard study, containing a <span class='a'>computer</span>, <span class='a'>desk</span> and various files.",
            {"top": "80%", "left": "23%"},
            ["study"],
            {"desk": new Inspectable("desk", "a wooden <span class='a'>desk</span> sitting to the side of the room", "A desk with two drawers, labeled '<span class='a'>Drawer 1</span>' and '<span class='a'>Drawer 2</span>'", ["desk"], this.state, true),
            "drawer 1": new Inspectable("Drawer 1", "drawer 1", "A draw labeled 'Drawer 1' inside it you find a copy of 'Goatee Growers Monthly' containing some good advice for a certain style of facial hair.", ["drawer 1"], this.state),
            "drawer 2": new Inspectable("Drawer 2", "drawer 2", "A draw labeled 'Drawer 2' inside you see a small tin of <span class='a'>tuna</span> which smells rather inviting.", ["drawer 2"], this.state),},
            {"tuna": new Collectable("Tuna", "a small tin of <span class='a'>tuna</span>", "A small can of tuna, smells pretty good.", ["tuna"], this.state)},
            {},
            this.state,
            "down",
            3,
            "You attempt to open the Study door, but the <span class='a'>study lock</span> keeping it shut. Maybe you can find the combination elsewhere.",
            {"left": "30%", "top": "79%"},
            "#study-cross"
            ),

            LOUNGE: new Room('the Lounge', "You walk into the dark room, lit only by the tv. On the armchair is Alex who has apparently been sitting, unmoving, for so long that he has fused with the sofa, mubling about port and chicken dinners." +
            "", "The main room",
            {"top": "75%", "left": "61%"},
            ["lounge"],
            {"port": new Inspectable("Port", "a tower of <span class='a'>port</span> bottles", "A truely titanic stack of port bottles, next to some weird little glass. Alex must have had a lot of chicken dinners.", ["port"], this.state, true),
            "armchair": new Inspectable("Armchair", "the <span class='a'>armchair</span> Alex inhabits", "An armchair that was once a light cream, now faded to a rather disturbing brown", ["armchair"], this.state, true),
            "alex": new Inspectable("Alex", "<span class='a'>Alex</span>, a now husk of a man sitting in the armchair", "He looks like he's spend the past few weeks, unmoving, in darkness, and eating nothing but Morrisons pies. He's mumbling about needing a lamp from the bathroom for sweet vitamin D.", ["alex"], this.state, true)},
            {"litter picker": new Collectable("Litter Picker", "a <span class='a'>litter picker</span>", "A long pole with a grabbing claw one and, and trigger on the other", ["litter picker"], this.state, true)},
            {},
            this.state,
            "down",
            7,
            "You try the door to the lounge, but it just rattles uselessly. There's a keyhole below the handle.",
            {"left": "49.9%", "top": "80%"},
            "#lounge-cross"
            ),

            KITCHEN: new Room('the Kitchen', "A lovely looking kitchen complete with breakfast bar, TV, and a couple of armchairs in the corner. Light is streaming in through the large windows. ",
            "A lovely looking kitchen complete with breakfast bar, TV, and a couple of armchairs in the corner. Light is streaming in through the large windows. ",
            {"top": "30%", "left": "41%"},
            ["kitchen"],
            {
            "kitchen table": new Inspectable("Kitchen Table", "a <span class='a'>kitchen table</span>", "A table that exists within the confines of the kitchen. Resting upon it appears to be a <span class='a'>fruit bowl</span>, a <span class='a'>lemon juicer</span>, and a <span class='a'>cotton bud</span>", ["table", "kitchen table"], this.state, true),
            "cotton bud": new Inspectable("A damp cotton bud", "a <span class='a'>cotton bud</span>", "A cotton bud with a slighty citrusy aroma to it.", ["cotton bud"], this.state),
            "fruit bowl": new Inspectable("A fruit bowl", "a <span class='a'>fruit bowl</span>", "A fruit bowl that seems entirely stocked by lemons and limes, apart from an unaturally sized tomato. What kind of weirdo needs a tomato this big?", ["fruit bowl"], this.state, true),
            "lemon juicer": new Inspectable("A lemon juicer", "a <span class='a'>lemon juicer</span>", "A lemon juicer with it's own reservoir containing a small amount of juice, there is half a lemon smushed into the top of it", ["lemon juicer"], this.state, true),
            "cupboard": new Inspectable("Cupboard", "some ransacked <span class='a'>cupboards</span> against the side wall", "You look through the cupboard and see a <span class='a'>page</span> from a notebook and a small bag of <span class='a'>sugar</span>.", ["cupboard", "cupboards"],
            this.state, true),
            "fridge": new Inspectable("Fridge", "the <span class='a'>fridge</span>", "A large fridge almost stripped bare, all that remains is a small tub of <span class='a'>sloes</span>.", ["fridge"], this.state, true)},
            {"blank page": new Collectable("Blank Page", "a blank page ripped out a notebook", "A blank page that appears to have slightly damp letter imprints in it, but they are not clear enough to read.", ["blank page", "page"],
            this.state, false),
            "sugar": new Collectable("Sugar", "a mostly empty bag of sugar", "A small, mostly empty bag of sugar.. you think.", ["sugar"], this.state, false),
            "sloes": new Collectable("Sloes", "", "A tub of small berries, the label on the outside says \"<span class='a'>Sloes</span>\".", ["sloes", "sloe"], this.state, false)},
            {},
            this.state,
            "down"
            ),

            TOILET: new Room("the toilet", "A small washroom ", "A small washroom",
            {"top": "58%", "left": "27%"},
            ["toilet"],
            {},
            {},
            {},
            this.state,
            "down",
            99,
            "You try the door, but it appears to be stuck.",
            {"left": "30%", "top": "57%"},
            "#toilet-cross"
            ),

            ALCOHOL: new Room('Alcohol Storage', "The room is stewn with bottles of wine, beer and every other form of booze imagineable. ", "The room is stewn with bottles of wine, beer and every other form of boozle imagineable.",
            {"top": "30%", "left": "58%"},
            ["alcohol", "alcohol storage"],
            {"wine rack": new Inspectable("Wine", "a wine rack against the back wall", "A rack containing a bottle of wine from every corner of the earth, even the ones with the very funny names.", ["wine rack"], this.state, true),
            "beer": new Inspectable("beer", "a pile of beers in the corner of the room", "A mound of the finest Badgers beer, any disturbance of this pile would create an avalanche of alcohol.", ["beer"], this.state, true ),
            "spirits cupboard": new Inspectable("Spirits Cupboard", "a <span class='a'>spirits cupboard</span> mounted below the wine rack", "You try the door on the cupboard but it's stuck. You need something to get it open", ["spirits cupboard", "cupboard"], this.state, true),
            "time machine": new Inspectable("Bottle Shaped Time Machine", "what is labeled as a 'miniature <span class='a'>time machine</span> [do not touch]'", "The note on it tells you not to touch it, but you cannot read, so such a warning cannot stop you. It appears to be a small time machine, in the shape of a bottle. What could this possibly be used for?",
            ["time machine"], this.state, true)},
            {},
            {"gin": new Container("Gin", "", {"sloes": false, "sugar": false}, ["Gin"], "A bottle of regular gin.", ["gin"], this.state, false)},
            this.state,
            "down"
            ),

            ALEX: new Room("Alex's Room",
            "There is no one in the room. ",
            "A standard bedroom.",
            {"left": "17%", "top": "65%"},
            ["alex's room", "alexs room"],
            {"computer": new Inspectable("Computer", "Alex's <span class='a'>PC</span>", "A spreadsheet is open on his PC, on it you find a concerning amount of data on penises.", ["pc"], this.state, true)},
            {"fursuit": new Inspectable("Fursuit", "a furry <span class='a'>unicorn costume</span>", "It looks like this has been the starring member in some furpiles.", ["fursuit", "unicorn costume"], this.state, true)},
            {},
            this.state,
            "up"
            ),

            THOM: new Room("Thom's Room", "", "Thom's Room",
            {"left": "31%", "top": "25%"},
            ["thoms room", "thom's room"],
            {}, 
            {"lounge key": new Collectable("Lounge Key", "the <span class='a'>lounge key</span>", "A key that may open the lounge.", ["lounge key", "Lounge Key"], this.state, true),},
            {},
            this.state,
            "up",
            6,
            "You attempt to enter Thom's room, but he is laying on the floor in a drunken stupor, throwing bits of dog bone at you and demanding you fetch him a bottle of sloe gin. He yeets a <span class='a'>crowbar</span> at you too, blabbering something about a stuck door, might be useful to pick up and take with you.",
            {"left": "34%", "top": "34%"},
            "#thom-cross"),

            LANDING: new Room("Landing", "It is the nexus of all rooms in the upstairs of the Ridgers'. You look at your map to see where you can go. ", "You look round the landing at the various rooms you can enter.",
            {"left": "34%", "top": "40%"},
            ["landing", "Landing"],
            {"chilli plants": new Inspectable("Chilli Plants", "Some attractive looking chilli plants", "Some gnarly looking chillis are growing", ["chilli plant"], this.state, true),
            "thom": new Inspectable("Thom", "<span class='a'>Thom<span> though the doorway of his room", "A man surrounded by many plants and empty bottles. He is looking sadly into his most recent empty bottle.", ["thom"], this.state, true)},
            {"crowbar": new Collectable("Crowbar", "It's a crowbar.", "For all your crowing needs.", ["crowbar"], this.state)},
            {},
            this.state,
            "up"),

            BATHROOM: new Room("the Bathroom", "It is room with many amenities to clean oneself.", "It is a room with many amenities to clean oneself.",
            {"left": "21%", "top": "40%"},
            ["bathroom"],
            {"distillery": new Inspectable("Distillery", "a distillation plant that Thom has apprently set up in the bath", "You look over to examine the distillery, but the fumes start making you light headed, so you step back to the entrance of the room.", ["distillery"], this.state, true),
            "bathtub": new Inspectable("Bathtub", "a <span class='a'>bathtub</span>", "You look into the bathtub and see something a <span class='a'>lamp</span>, but the fumes stop you from getting close.", ["bathtub"], this.state, true),
            "toilet": new Inspectable("Toilet", "a <span class='a'>toilet</span>", "You peer into the toilet and recoil in horror at the brown slurry that fills the bowl.", ["toilet"], this.state, true),
            "shower": new Inspectable("Shower", "the <span class='a'>shower", "A bone dry shower that looks like it hasn't been used in a long while.", ["shower"], this.state, true),
            "lamp": new Inspectable("Lamp", "a strange shape floating in the tub", "You try to get a better look, but the fumes stop you from getting much closer. It appears to be a light of sorts.", ["lamp"], this.state, true)},
            {"uv lamp": new Collectable("UV Lamp", "", "A lamp that provides essential vitamin D to those in desolate parts of the world with little sun, or autistic gamers that don't go outside.", ["uv lamp"], this.state, false)},
            {},
            this.state,
            "up"
            ),

            OBJECTS: new Room("", "", "", "", "", {}, {
            "upstairs map": new Collectable("Upstairs Map", "a map of the upstairs of the house", "", [], this.state),
            "study code": new Collectable("Study Code", "", "", ["study code"], this.state, false, "#study-code-note-wrapper"),
            "sloe gin": new Collectable("Sloe Gin", "", "A bottle of futuristically aged sloe gin. Looks pretty tasty.", ["sloe gin", "gin"], this.state),
            "front door key": new Collectable("Front Door Key", "", "A key that should open the front door lock", ["front door key"], this.state)
            })
        }
        this.state.playerRoom = this.rooms.HALL;
        


        $(".modal-close").click(() => {
            this.state = this.state.setState();
            this.stateHandler();
        });


        $("#study-code-note").html("One digit is right and in its place ->                   [6, 8, 2]\n\n"+
        "One digit is right but in the wrong place ->             [6, 1, 4]\n\n"+
        "Two digits are right, but both are in the wrong place -> [2, 0, 6]\n\n"+
        "All digits are wrong ->                                  [7, 8, 3]\n\n"+
        "One digit is right, but in the wrong place ->            [3, 8, 0]");
        $("#study-code-close").click(() => {
            $("#study-code-note-wrapper").css("display", "none");
        })
        this.input.keypress(e => {
            if (e.keyCode === 13) {
                this.inputHandler(this.input.val());
                this.input.val("");
            }
        })

        //Initial game setup
        this.output.addOutput("Available Commands: help, move, pick up, look, use, examine/ex")
        this.output.addOutput("Usage: help (shows this), move [location], pick up [item], look (describes current room), use [item] on [other item], examine/ex [item]")
        this.output.addOutput("You wake up in the entrance to the Ridgers' residence. The <span class='a'>front door</span> is jammed shut. The stairs are being guarded by two <span class='a'>cats</span>, one of whom looks slightly tarded. " + 
        "You attempt to go up, but they viciously attack your weak frame. There is a small scrap of <span class='a'>paper</span> on the floor in front of you with some scribbling on.")
        $('#downstairs-marker').css({"top": "70%", "left": "35%"});

        //automated test input
        //this.test();

    }

    test() {
        let inputs = ["use tuna on cats", "move thoms room", "pick up crowbar", "move alcohol", "use crowbar on spirits cupboard", "pick up gin", "move kitchen", "pick up sloes",
    "pick up sugar", "use sugar on gin", "use sloes on gin", "move alcohol", "use gin on time machine", "move landing", "use sloe gin on thom", "move thoms room", "pick up lounge key", "move hall",
    "use lounge key on lounge lock", "move lounge", "pick up litter picker", "move bathroom", "use litter picker on lamp", "move lounge", "use uv lamp on alex", "move hall", "use front door key on front door lock"];
        let inputElem = $("#text-input");
        $("#testing-input").click(() => {
            this.inputHandler(this.input.val());
        })
        for (let i=0; i<inputs.length; i++) {
            setTimeout(() => inputElem.val(inputs[i]), i*200);
            setTimeout(() => $("#testing-input").click(), i*201);
        }
    }

    canCollect(toCollect) {
        let output = false;
        if (Object.values(this.state.getCurrentRoom().containers).indexOf(toCollect) != -1) {
            output = !this.state.getCurrentRoom().containers[toCollect.name.toLowerCase()].inInventory;
        } else if (Object.values(this.state.getCurrentRoom().collectables).indexOf(toCollect) != -1) {
            output = !this.state.getCurrentRoom().collectables[toCollect.name.toLowerCase()].inInventory;
        }
        return output;
    }

    getInspectableFromAlias(alias) {
        let output = false;
        let inspectables = this.state.getCurrentRoom().inspectables;
        Object.keys(inspectables).forEach(key => {
            let inspectable = inspectables[key];
            if (inspectable.aliases.includes(alias.toLowerCase())) {
                output = inspectable;
            }
           
        })
        return output;
    }

    collectableInInventory(alias) {
        let collectables = this.state.playerInventory;
        for (let i=0; i<collectables.length; i++) {
            if (collectables[i].aliases.includes(alias.toLowerCase())) {
                return true;
            }
        return false;
        };
    }

    getInventoryItemFromAlias(alias) {
        let output = false;
        let inventory = this.state.playerInventory;
        Object.keys(inventory).forEach(key => {
            let item = inventory[key];
            if (item.aliases.includes(alias.toLowerCase())) {
                output = item;
            }
        })
        return output;
    };
    

    getCollectableFromAlias(alias) {
        let output = false;
        let collectables = this.state.getCurrentRoom().collectables;
        Object.keys(collectables).forEach(key => {
            let collectable = collectables[key];
            if (collectable.aliases.includes(alias.toLowerCase())) {
                output = collectable;
            }
        })
        return output;
    }

    getContainerFromAlias(alias) {
        let output = false;
        Object.keys(this.rooms).forEach(key => {
            let containers = this.rooms[key]["containers"];
            if (Object.keys(containers).length > 0) {
                Object.keys(containers).forEach(key => {
                    let container = containers[key];
                    if (container.aliases.includes(alias.toLowerCase())) {
                        output = container;
                    }
                })
            }
        });
        return output;
    }

    getObjectFromAlias(alias) {
        let item = this.getInspectableFromAlias(alias);
        if (!item) {
            item = this.getCollectableFromAlias(alias);
        }
        if (!item) {
            item = this.getContainerFromAlias(alias);
        }
        return item;
    }

    isValidRoom(input) {
        let isValid = false;
        Object.keys(this.rooms).forEach(key => {
            if (this.rooms[key].aliases.includes(input))  {
                isValid = true;
            }
            
        })
        return isValid;
    }

    getRoomFromAlias(input) {
        let roomObj = false;
        Object.keys(this.rooms).forEach(key => {
            if (this.rooms[key].aliases.includes(input.toLowerCase()))  {
                roomObj = this.rooms[key];
            }
            
        })
        return roomObj;
    }

    inputFormatter(input) { 
        let cmd = input.toLowerCase().split(" ");
        let toStrip = ["to", "the", "at", "use"];
        for (let i=0; i<toStrip.length; i++) {
            let word = cmd.indexOf(toStrip[i]);
            if (word != -1) {
                cmd.splice(word, 1);
            }
        }
        return cmd;
    }

    useOnFormatter(input) {
        let split = input.split(" on ");
        let x = split[0].split(" ");
        let word = x.indexOf("use");
        if (word != -1) {
            x.splice(word, 1);
        }
        return [x.join(" ").trim(), split[1].trim()]
    }


    useOnHandler(x, y) {
        console.log(x, y)
        if (x == this.rooms.KITCHEN.collectables["blank page"] && y == this.rooms.HALL.inspectables["radiator"]) {
            this.state.addToPuzzlesCompleted("Note");
        } else if (x == this.rooms.STUDY.collectables["tuna"] && y == this.rooms.HALL.inspectables["cats"]) {
            this.state.addToPuzzlesCompleted("Cats");
        } else if (x == this.rooms.LANDING.collectables["crowbar"] && y == this.rooms.ALCOHOL.inspectables["spirits cupboard"]) {
            this.state.addToPuzzlesCompleted("Gin");
        } else if (x == this.rooms.OBJECTS.collectables["sloe gin"] && y == this.rooms.LANDING.inspectables["thom"]) {
            this.state.addToPuzzlesCompleted("Sloe Gin");
        } else if (x == this.rooms.THOM.collectables["lounge key"] && y == this.rooms.HALL.inspectables["lounge lock"]) {
            this.state.addToPuzzlesCompleted("Lounge");
        } else if (x == this.rooms.LOUNGE.collectables["litter picker"] && y == this.rooms.BATHROOM.inspectables["lamp"]) {
            this.state.addToPuzzlesCompleted("UV");
        } else if (x == this.rooms.BATHROOM.collectables["uv lamp"] && y == this.rooms.LOUNGE.inspectables["alex"]) {
            this.state.addToPuzzlesCompleted("Front");
        } else if (x == this.rooms.OBJECTS.collectables["front door key"] && y == this.rooms.HALL.inspectables["front door lock"]) {
            this.state.addToPuzzlesCompleted("Complete");
            //------------------------ SLOE GIN HANDLING --------------------------------------------------//
        } else if (x == this.rooms.KITCHEN.collectables["sugar"] && y == this.rooms.ALCOHOL.containers["gin"]) {
            y.contents["sugar"] = true;
            y.nameList.push("Sugar");
            this.state.removeFromInventory(this.rooms.KITCHEN.collectables["sugar"]);
            y.updateName();
        } else if (x == this.rooms.KITCHEN.collectables["sloes"] && y == this.rooms.ALCOHOL.containers["gin"]) {
            y.contents["sloes"] = true;
            y.nameList.push("Sloes");
            this.state.removeFromInventory(this.rooms.KITCHEN.collectables["sloes"]);
            y.updateName();
        } else if (x == this.rooms.ALCOHOL.containers["gin"] && y == this.rooms.ALCOHOL.inspectables["time machine"]) {
            if (x.contents["sloes"] === true && x.contents["sugar"] === true) {
                this.output.addOutput("You place the bottle of gin into the similarly shaped time machine and press the 'Go' button on top. It shakes a bit and displays that it has advanced 6 months. You open it up and find your bottle of gin, sloes and sugar has transformed into delicious sloe gin.")
                this.state.removeFromInventory(this.rooms.ALCOHOL.containers["gin"]);
                this.state.addToInventory(this.rooms.OBJECTS.collectables["sloe gin"]);
            } else {
                this.output.addOutput("You feel like something is missing from the gin mix, maybe there is something around you need to put in.");
            }
        } else if (x == this.rooms.ALCOHOL.containers["gin"] && y == this.rooms.LANDING.inspectables["thom"]) {
            if (x.contents["sugar"] == true && x.contents["sloes"] == true) {
                this.output.addOutput("Thom looks at you in disgust and tells you to come back when you've properly aged the gin.");
            } else {
                this.output.addOutput("Thom looks at the gin bottle and tells you to find more shit to put in it.");
            }
            //------------------------ /SLOE GIN HANDLING --------------------------------------------------//
        }
        
       
    }

    roomDescriptionFormatter(description) { //Takes a room description array to format
        for (let i=description.length-1; i>1; i--) {
            if (i == description.length-1) {
                description.splice(i, 0, ", and ");
            } else {
                description.splice(i, 0, ", ")
            }
        }
        description.splice(1, 0, "You see ")
        return description.join("");
    }


    inputHandler(input) {
        if (/help/i.test(input)) {
            this.output.addOutput("Available Commands: help, move, look, look at, use");

        } else if (/look/i.test(input)) {
            if (input !== "look") {
                this.output.addOutput("Look is used to descibe your current room. To examine an item, type 'examine [item]'");
            }
            let roomDescription = this.roomDescriptionFormatter(this.state.getCurrentRoomDescription());
            this.output.addOutput(roomDescription + ".");

        } else if (/^examine|^ex/i.test(input)) {
            let cmd = this.inputFormatter(input);
            let object = cmd.slice(1, cmd.length).join(" ");
            let item = this.getObjectFromAlias(object);
            if (!item) {
                item = this.getInventoryItemFromAlias(object);
                if (!item) {
                    this.output.addOutput("You cannot examine " + object);
                    return;
                }
            }
            this.output.addOutput(item.getLookDescription());
            if (item.elemToShowOnExamine !== false) {
                item.onExamine();
            }
            
        } else if (/pick\s+up/i.test(input)) {
            let cmd = this.inputFormatter(input);
            let object = cmd.slice(2, cmd.length).join(" ");
            let item = this.getObjectFromAlias(object);
            if (this.canCollect(item)) {
                this.state.addToInventory(item);
                this.output.addOutput("You have picked up " + item.name);
            } else {
                this.output.addOutput("You are not able to pick up " + object + ".");
            }
        } else if (/^move/i.test(input)) {
            let cmd = this.inputFormatter(input);
            if (cmd.length === 1) {
                this.output.addOutput("You need to enter a location to move to. -- \"Move to the kitchen\"");
                return;
            }
            let command = cmd[0];
            let location = cmd.slice(1, command.length-1).join(" ");
            let room = this.getRoomFromAlias(location);
            if (room == this.state.getCurrentRoom()) {
                this.output.addOutput("You are already in " + room.name + ".");
                return;
            }
            if (room.stateToAccess > this.state.getStateNumber()) {
                this.output.addOutput(room.deniedAccessMessage);
                room.showDoorCross();
            } else if (this.isValidRoom(location)) {
                this.state.setCurrentRoom(room);
                let roomDescription = this.roomDescriptionFormatter(this.state.getCurrentRoomDescription());
                this.output.addOutput("You walk to " + this.state.getCurrentRoom().name + ". \n" + roomDescription);
                this.state.playerRoom.setMarker();
            } else {
                this.output.addOutput(location + " is not a valid room to move to.")
            }
        } else if (/use/i.test(input)) {
            let cmd = this.useOnFormatter(input);
            console.log(cmd);
            let use = cmd[0];
            console.log(use)
            let useObj = this.getInventoryItemFromAlias(use);
            if (!useObj) {
                this.output.addOutput("You do not have " + use + " in your inventory.");
                return;
            }
            let on = cmd[1];
            let onObj = this.getObjectFromAlias(on);
            if (!onObj) {
                this.output.addOutput("You cannot see a(n) " + on + " to use your " + use + " on.")
                return;
            }
            this.output.addOutput("You use " + use + " on " + on);
            this.useOnHandler(useObj, onObj);
        }
        this.state = this.state.setState();
        this.stateHandler();
        console.log(this.state)
    }

    //State manager
    stateHandler() {
        if (this.state.getStateNumber() === 1) {
            if (!this.state.stateRun) {
                $('#downstairs-map-wrapper').css('display', 'inline-block');
                this.state.stateRun = true;
            }
        } else if (this.state.getStateNumber() === 2) {
            if (!this.state.stateRun) {
                this.output.addOutput("You see some writing appearing on the once blank page, perhaps it's a clue...");
                this.state.removeFromInventory(this.rooms.KITCHEN.collectables["blank page"]);
                this.state.addToInventory(this.rooms.OBJECTS.collectables["study code"]);
                this.state.stateRun = true;
            }
        } else if (this.state.getStateNumber() === 3) {
            if (!this.state.stateRun) {
                this.output.addOutput("The now open lock falls to the ground, and the study door swings open.");
                $("#study-cross").css("display", "none");
                this.state.removeFromInventory(this.rooms.OBJECTS.collectables["study code"]);
                this.state.stateRun = true;
            }
        } else if (this.state.getStateNumber() === 4) {
            if (!this.state.stateRun) {
                $('#upstairs-map-wrapper').css('display', 'inline-block');
                this.state.removeFromInventory(this.rooms.STUDY.collectables["tuna"]);
                this.output.addOutput("You see another scrap of paper that the tardy cat was sitting on, and it seems to be a map of the upstairs. You look up at the stairs, and decide to ascend.");
                $("#upstairs-marker").css({"left": "38%", "top": "40%"})
                this.state.stateRun = true;
                this.inputHandler("move landing");
            }
        } else if (this.state.getStateNumber() === 5) {
            if (!this.state.stateRun) {
                this.output.addOutput("You pry open the spirits cupboard, and see a bottle of <span class='a'>gin</span> sitting inside.")
                this.state.removeFromInventory(this.rooms.LANDING.collectables["crowbar"]);
                this.state.stateRun = true;
            }
        } else if (this.state.getStateNumber() === 6) {
            if (!this.state.stateRun) {
                this.state.stateRun = true;
                $("#thom-cross").css("display", "none");
                this.state.removeFromInventory(this.rooms.OBJECTS.collectables["sloe gin"]);
                this.output.addOutput("Thom is distracted by his new bottle of delicious gin, and stops hurling things at you. You can probably go in his room now.")
            }
        } else if (this.state.getStateNumber() === 7) {
            if (!this.state.stateRun) {
                $("#lounge-cross").css("display", "none");
                this.state.removeFromInventory(this.rooms.THOM.collectables["lounge key"]);
                this.state.stateRun = true;
            }
        } else if (this.state.getStateNumber() === 8) {
            if (!this.state.stateRun) {
                this.output.addOutput("You fish a what seems to be a UV lamp out the alcoholic tub.");
                this.state.addToInventory(this.rooms.BATHROOM.collectables["uv lamp"]);
                this.state.stateRun = true;
            }
        } else if (this.state.getStateNumber() === 9) {
            if (!this.state.stateRun) {
                this.output.addOutput("Alex thanks you for helping to cure his horrible rickets, and in return grants you the <span class='a'>front door key</span> to finally leave the house.")
                this.state.removeFromInventory(this.rooms.BATHROOM.collectables["uv lamp"]);
                this.state.addToInventory(this.rooms.OBJECTS.collectables["front door key"]);
                this.state.stateRun = true;
            }
        } else if (this.state.getStateNumber() === 10) {
            if (!this.state.stateRun) {
                this.output.addOutput("CONGRATULATIONS!");
                this.output.addOutput("You have finally escape that weird hellhole, and stumble out the door.");
                this.state.stateRun = true;
            }
        }
    }

}


$(document).ready(() => {
    var game = new Game();
    $('#text-input').focus();
    $("#text-output").click(() => {
        $('#text-input').focus();
    })
    console.log("Close your devtools you autists.");
})
